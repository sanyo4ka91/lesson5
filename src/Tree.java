import java.util.Objects;

public class Tree {

    public int getHeight() {
        return height;
    }

        public void setHeight(int height) {
            this.height = height;

    }

    private int height;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tree tree = (Tree) o;
        return hashCode() == tree.hashCode();

    }

    @Override
    public int hashCode() {
        return Objects.hash(height, name);
    }

    @Override
    public String toString() {
        return "Tree{" +
                "height=" + height +
                ", name='" + name + '\'' +
                '}';
    }
}
